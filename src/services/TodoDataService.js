import { db, auth } from '../firestore'

const user = auth.currentUser

const collection = db.collection('users').doc(user.uid).collection('Todo')

class TodoDataService {
  getAll() {
    return collection.get()
  }

  create(todo) {
    return collection.add(todo)
  }

  update(id, value) {
    return collection.doc(id).update(value)
  }

  delete(id) {
    return collection.doc(id).delete()
  }
}

export default new TodoDataService()
