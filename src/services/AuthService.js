import { getAuth, signOut, createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'firebase/auth'

const auth = getAuth()

class AuthService {
  signIn(email, password) {
    return signInWithEmailAndPassword(auth, email, password)
  }

  register(email, password) {
    return createUserWithEmailAndPassword(auth, email, password)
  }

  logout() {
    return signOut(auth)
  }
}

export default new AuthService()
