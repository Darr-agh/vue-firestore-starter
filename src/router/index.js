import { createRouter, createWebHistory } from 'vue-router'
import firebase from '../firestore.js'

const routes = [
  {
    path: '/',
    name: 'Login',
    meta: { requiresAuth: false },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "Login" */ '../views/Login.vue')
  },
  {
    path: '/app',
    name: 'AppPage',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "Layout" */ '../views/Layout.vue'),
    children: [
      {
        path: '',
        name: 'Todo',
        meta: { requiresAuth: true },
        component: () => import(/* webpackChunkName: "Todo" */ '../views/Todo.vue')
      }
    ]
  }
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

// Check if user is signed in before each route, if not return home

router.beforeEach(async (to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  if (requiresAuth && !await firebase.getCurrentUser()){
    next('/')
  }else{
    next()
  }
})

export default router
