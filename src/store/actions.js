const actions = {
  addUser ({ commit }, user) {
    commit('setUser', user)
  }
}

export default actions