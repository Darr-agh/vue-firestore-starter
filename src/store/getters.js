const getters = {
  uid: state => {
    return state.user.user.uid
  }
}

export default getters