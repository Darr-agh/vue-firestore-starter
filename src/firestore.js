import firebase from 'firebase/compat/app'
import 'firebase/compat/firestore'
import 'firebase/compat/auth'

let firebaseConfig = {
  apiKey: 'AIzaSyCNqbxMPbT9R0dgswgeyziQdO2YguSfDtU',
  authDomain: 'canibuy-io.firebaseapp.com',
  databaseURL: 'https://canibuy-io.firebaseio.com',
  projectId: 'canibuy-io',
  storageBucket: 'canibuy-io.appspot.com',
  messagingSenderId: '615781327285',
  appId: '1:615781327285:web:f3a660a5bf71ae50'
}

firebase.initializeApp(firebaseConfig)

firebase.getCurrentUser = () => {
  return new Promise((resolve, reject) => {
      const unsubscribe = firebase.auth().onAuthStateChanged(user => {
          unsubscribe()
          resolve(user)
      }, reject)
  })
}

// utils
const db = firebase.firestore()
const auth = firebase.auth()

export { db, auth }

export default firebase 