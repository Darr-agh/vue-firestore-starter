import { createApp } from 'vue'
import './firestore'
import router from './router'
import App from './App.vue'
import store from './store'

// Tailwind
import './assets/tailwind.css'
import './assets/styles/main.scss'

createApp(App).use(store).use(router).mount('#app')
