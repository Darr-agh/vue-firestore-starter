module.exports = {
  extends: ['stylelint-config-sass-guidelines'],
  ignoreFiles: ['**/*.vue'],
  rules: {
    indentation: 2,
    'string-quotes': 'single',
    'color-hex-case': 'upper',
    'color-hex-length': 'long',
    'selector-combinator-space-after': 'always',
    'selector-attribute-quotes': 'always',
    'selector-attribute-operator-space-before': 'never',
    'selector-attribute-brackets-space-inside': 'never',
    'declaration-block-trailing-semicolon': 'always',
    'declaration-colon-space-before': 'never',
    'declaration-colon-space-after': 'always',
    'number-leading-zero': 'never',
    'function-url-quotes': 'always',
    'font-weight-notation': 'numeric',
    'font-family-name-quotes': 'always-unless-keyword',
    'rule-empty-line-before': 'always-multi-line',
    'selector-pseudo-element-colon-notation': 'single',
    'selector-pseudo-class-parentheses-space-inside': 'never',
    'order/properties-alphabetical-order': null,
    'max-nesting-depth': 4,
    'selector-no-qualifying-type': null,
    'selector-max-compound-selectors': 5,
    'selector-class-pattern': null,
    'order/order': null,
    'scss/at-rule-no-unknown': null
  }
}
