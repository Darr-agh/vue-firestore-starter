module.exports = {
  root: true,
  parserOptions: {
    'parser': 'babel-eslint',
    'sourceType': 'module',
    'ecmaFeatures': {
      'impliedStrict': true
    }
  },
  env: {
    'browser': true,
    'commonjs': true,
    'node': true,
    'es2017': true,
    'serviceworker': false
  },
  extends: [
    'plugin:vue/vue3-essential',
    'plugin:vue/vue3-strongly-recommended',
    'eslint:recommended'
  ],
  globals: {
    process: true
  },
  plugins: [
    'vue' // eslint-plugin-vue
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  // add your custom rules here
  rules: {
    // https://eslint.org/docs/rules/ - 'off'/0, 'warn'/1 or 'error'/2
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'warn', // disallow the use of debugger
    quotes: [
      'error',
      'single',
      { avoidEscape: true, allowTemplateLiterals: true }
    ],
    'comma-dangle': ['error', 'never'],
    'semi': ['error', 'never'],
    'quote-props': 0,
    'no-trailing-spaces': 0,
    'vue/component-name-in-template-casing': ['error', 'kebab-case'],
    'vue/mustache-interpolation-spacing': ['error'],
    'vue/singleline-html-element-content-newline': 'off', // Require a line break before and after the contents of a singleline element
    'vue/html-self-closing': 0,
    'vue/html-closing-bracket-spacing': 0,
    'vue/multiline-html-element-content-newline': 0,
    'vue/comma-dangle': ['error', 'never'],
    'vue/no-unused-components': 0,
    'vue/no-v-html': 0,
    'vue/require-default-prop': 0,
    'vue/component-definition-name-casing': 0,
    'unicorn/prefer-includes': 0,
    'vue/max-attributes-per-line': [
      'warn',
      {
        'singleline': 4,
        'multiline': {
          'max': 1,
          'allowFirstLine': false
        }
      }
    ]
  }
}
